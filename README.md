
### INSTALLATION ###


#### Install Cordova in Ubuntu/Linux ####
Follow these steps to install Cordova in ubuntu:

1. Install Node.js. Node.js may already be installed. To test the installation, open a terminal window and executing below command. If the version number is displayed, Node.js is installed.

        nodejs --version

2. If Node.js is not installed, install with:
    
        sudo apt-get install nodejs

3. Also add the alias "node" for nodejs, as this is needed by Cordova:
    
        sudo ln -s /usr/bin/nodejs /usr/bin/node

4. Install Node Package Manager (npm). Test if npm is installed by executing below command.
    
        npm --version    
        
    If npm is not installed, install by executing following command:
        
        sudo apt-get install npm

5. Install Git. Git is a version control system, which is used by Cordova behind-the-scenes. Test if Git is installed by executing below command.
        
        git --version   
        
    If Git is not installed, install by executing the below command:
        
        sudo apt-get install git

6. Install Cordova. 
    Cordova is installed using the Node Package Manager (npm). Type the following to install:
    
        sudo npm install -g cordova@8.0.0

7. Test the Cordova install by executing the below code. If you see the version number, you have successfully installed Apache Cordova! 
    
        cordova --version

8. Install Java in Ubuntu, Add Oracle's PPA, then update your package repository.		

        sudo add-apt-repository ppa:webupd8team/java
        sudo apt-get update

9. Download and install the installer script. Install Java 1.8 version.

        sudo apt-get install oracle-java8-installer
        
    Once the installation is complete execute following command to test.

        java --version

10. Install Android Studio.
    Download Android studio zip for your machine from https://developer.android.com/studio/ 
    Unzip the file and move the folder into /usr/local/
    
        unzip android-studio-ide-173.4720617-linux.zip

    Navigate to /usr/local/android-studio/bin and run below command to launch android studio.
    
        ./studio.sh

11. Install Gradle 4.7 
    Install SDKMAN if it is not installed already.
    
        curl -s “https://get.sdkman.io” | bash        
        sdk install gradle 4.7


### DEPLOYING THE FRAMEWORK ###

1. Clone the Repository
    Open command prompt and execute the below command
    
        git clone https://rakeshsv@bitbucket.org/issecudayton/hybridguard2018.git
        
    Note: The link may vary depending on the username.

2. Change branch from master to dev by executing below command.
        git checkout dev_
        git branch_

3. Navigate to gallery folder and execute below code to prepare cordova project in your local machine.

        cordova prepare

    Run below commands to verify platforms and plugins

        cordova platform list
        cordova plugin list

4. Import the project into Android Studio.
    Note: If build fails follow Issues and Resolution section.

5. Add Virtual Device in AVD manager in android studio.
  1. Choose the required android phone model and click Next.
  2. Choose a system image version > 26 and click download.
  3. Click Run button. \
  4. Choose the emulator or the device you want to deploy.


### Issues and Resolutions ###

1.  Give proper path to the file in your machine, example given below. change path accordingly.
    
        sudo chmod +x /Users/sunkaralakuntavenkr1/Documents/Research/HG/hybridguard2018/prototype/apps/gallery/platforms/android/gradlew

2. Failed to find target with hash string 'android-26' in: /home/hybridguard/Android/Sdk
    Install missing platform(s) and sync project
    Click on the link in android-studio and it will automatically download the missing android apk.

3. All flavors must now belong to a named flavor dimension. Learn more at
        
        https://d.android.com/r/tools/flavorDimensions-missing-error-message.html
    
    Add the below lines of code under “android” tag in build.gradle file.

        flavorDimensions "tier"
        productFlavors {
            free {
                // Assigns this product flavor to the "tier" flavor dimension. Specifying
                // this property is optional if you are using only one dimension.
                dimension "tier"
            }
        }

4. Configuration 'compile' is obsolete and has been replaced with 'implementation' and 'api'. For more information see: http://d.android.com/r/tools/update-dependency-configurations.html
    Replace “compile” with “implementation” in build.gradle file.

5. Device supports x86, but APK only supports arm64-v8a

    build.gradle: in Module: app
    Add “x86” in 
    
        arm64 {
            versionCode defaultConfig.versionCode*10 + 3
            ndk {
                abiFilters = ["arm64-v8a", "x86"]
            }
        }

6. AAPT2 error: check logs for details
    
    AAPT2 is enabled by default when we use android plugin for gradle to improve incremental resource processing. Run below command.
    
        cordova plugin add cordova-android-support-gradle-release@1.4.2  --save
